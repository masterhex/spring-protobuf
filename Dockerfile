FROM java:8-jre

COPY ./entrypoint.sh /usr/src/app/entrypoint.sh
ADD ./app_config/docker /usr/src/app/
RUN chmod +x /usr/src/app/entrypoint.sh
RUN useradd -ms /bin/bash admin

ADD ./target/spring-protobuf.jar /usr/src/app/spring-protobuf.jar
RUN mkdir -p /usr/src/app
RUN chown -R admin:admin /usr/src/app
RUN chmod 755 /usr/src/app
USER admin

EXPOSE 8080

ENTRYPOINT ["/usr/src/app/entrypoint.sh"]