#!/bin/bash
ROOT_PATH=/usr/src/app
EXE_FILE=$ROOT_PATH/spring-protobuf.jar

java -jar \
-Denv=dev \
-Xms256m \
-Xmx512m \
-XX:MaxMetaspaceSize=512m \
-Dserver.port=8080 \
-Dserver.address=0.0.0.0 \
-Dspring.boot.admin.url=http://127.0.0.1:8099 \
-Dspring.boot.admin.client.preferIp=true \
-Dconfig_path=$ROOT_PATH \
-Dlogging.config=$ROOT_PATH/logback.xml $EXE_FILE
