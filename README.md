### Tech stack

* Spring Boot.
* Protocol buffers

### Build and execution 

Just execute `./exe.sh` file<br /> 
_(Docker compose / Docker/ Maven should be installed)_
<br /> <br /> 
If you want to run tests:<br /> 
`mvn clean compile test`<br /> 
If you need to rebuild protobuf model:<br /> 
`./protoc --java_out=... model.proto`

### The resources
* POST http://localhost:8080/protomodel<br /> - post ListOfModels
* GET http://localhost:8080/protomodel/{id}<br /> - get Model
* GET http://localhost:8080/protomodel<br /> - get all Models

ListOfModels : `{"model":[{"id":1,"name":"name"}]}`

### Short description
* Docker compose file is placed in the folder 
of the project only for the simplicity purpose.<br />
* Swagger wasn't included to make the project lighter<br />
* No unit tests, no logic there<br />
* Logs in the /tmp/protobuf-logs<br />
* Please not hesitate to contact me nurlan.profi@gmail.com<br />