package com.mycompany.protobuf.controller;


import com.googlecode.protobuf.format.JsonFormat;
import com.mycompany.protobuf.mapper.ModelOuterClass.*;
import com.mycompany.protobuf.service.ProtoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/protomodel")
public class ProtoController {

    @Autowired
    private ProtoService protoService;
    @Autowired
    private JsonFormat format;

    @PostMapping("")
    public ResponseEntity post(@RequestBody ListOfModels listOfModels) {
        protoService.postModel(listOfModels);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<String> get(@PathVariable Integer id) {
        Model model = protoService.getModel(id);
        return model != null ? ResponseEntity.ok(format.printToString(model)) : ResponseEntity.notFound().build();
    }

    @GetMapping("")
    public ResponseEntity<String> getAll() {
        ListOfModels list = protoService.getAll();
        return ResponseEntity.ok(format.printToString(list));
    }
}
