package com.mycompany.protobuf.util;

import com.mycompany.protobuf.mapper.ModelOuterClass.ListOfModels;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class FileUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    public static void writeToFile(ListOfModels listOfModels, String filePath) {

        createFileIfNotExists(filePath);

        try (InputStream inputStream = new FileInputStream(filePath)) {

            ListOfModels.Builder allModelsBuilder = ListOfModels.newBuilder().mergeFrom(inputStream);

            ListOfModels allModels = allModelsBuilder.addAllModel(listOfModels.getModelList()).build();
            LOGGER.debug("All models found in file - {}", allModels.toString());

            OutputStream output = new FileOutputStream(filePath);
            allModels.writeTo(output);
            output.flush();
            output.close();
        } catch (IOException e) {
            LOGGER.error("Something went wrong writing list to file - {}", e.getMessage());
        }
    }


    public static ListOfModels getListFromFile(String filePath) {
        createFileIfNotExists(filePath);
        ListOfModels existingModels = ListOfModels.getDefaultInstance();
        try (InputStream inputStream = new FileInputStream(filePath)) {
            existingModels = ListOfModels.parseFrom(inputStream);
        } catch (IOException e) {
            LOGGER.error("Something went wrong getting list from file - {}", e.getMessage());
            return existingModels;
        }
        return existingModels;
    }

    public static void createFileIfNotExists(String filePath) {
        LOGGER.info("creating file");
        File file = new File(filePath);
        try {
            if (!file.exists()) {
                if(!file.createNewFile()){
                    LOGGER.error("Something went wrong with creating the file {}", filePath);
                    throw new IOException("Something went wrong with creating the file");
                }
            }
        } catch (IOException e) {
            LOGGER.error("Something went wrong with creating file - {}", e.getMessage());
        }
        LOGGER.info("File created - {}", filePath);
    }

    public static void deleteFileIfExists(String filePath) throws IOException{
        File file = new File(filePath);

        if (file.exists()) {
            if (!file.delete()){
                LOGGER.error("Something went wrong with deleting the file file {}", filePath);
                throw new IOException("Something went wrong with deleting the file");
            }
        }
        LOGGER.info("File deleted - {}", filePath);
    }
}
