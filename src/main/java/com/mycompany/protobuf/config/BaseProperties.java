package com.mycompany.protobuf.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public abstract class BaseProperties {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseProperties.class);

    protected static Properties getProperties(String propertyName) {
        Properties properties = new Properties();
        StringBuilder stringBuilder = new StringBuilder(System.getProperty("config_path")).append("/")
                .append(propertyName).append(".properties");
        String configPath = stringBuilder.toString();

        stringBuilder = null;

        File file = new File(configPath);
        try (FileInputStream in = new FileInputStream(file)){
            properties.load(in);
        } catch (IOException e) {
            LOGGER.error("Error while loading properties: {}", e.getMessage(), e);
        }

        return properties;
    }
}
