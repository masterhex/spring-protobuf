package com.mycompany.protobuf.config;

import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class FileProperties extends BaseProperties {
    private static final String PROPERTY_FILE = "file";

    private static final String FILE_PATH = "file_path";


    public String getFilePath() {
        return getProperties().getProperty(FILE_PATH);
    }

    private static Properties getProperties() {
        return getProperties(PROPERTY_FILE);
    }
}
