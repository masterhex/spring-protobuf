package com.mycompany.protobuf.service;


import com.mycompany.protobuf.mapper.ModelOuterClass.*;

public interface ProtoService {
    void postModel(ListOfModels listOfModels);
    Model getModel(Integer id);
    ListOfModels getAll();
}
