package com.mycompany.protobuf.service.impl;

import com.mycompany.protobuf.config.FileProperties;
import com.mycompany.protobuf.exception.ConflictException;
import com.mycompany.protobuf.mapper.ModelOuterClass;
import com.mycompany.protobuf.mapper.ModelOuterClass.ListOfModels;
import com.mycompany.protobuf.mapper.ModelOuterClass.Model;
import com.mycompany.protobuf.service.ProtoService;
import com.mycompany.protobuf.util.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProtoServiceImpl implements ProtoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProtoServiceImpl.class);
    @Autowired
    private FileProperties fileProperties;

    @Override
    public void postModel(ListOfModels listOfModels) {
        LOGGER.info("Post is started");
        LOGGER.debug("Post list = {}", listOfModels);
        checkForExisting(listOfModels);
        FileUtil.writeToFile(listOfModels, fileProperties.getFilePath());
        LOGGER.info("Post is complete");
    }

    @Override
    public Model getModel(Integer id) {
        LOGGER.info("Get model by  id = {}", id);
        ListOfModels listOfModels = getAll();
        Model res = listOfModels.getModelList().stream().filter(s-> s.getId()==id).findFirst().orElse(null);
        LOGGER.debug("Model from list = {}", res);
        return res;
    }

    @Override
    public ListOfModels getAll() {
        ListOfModels listOfModels = FileUtil.getListFromFile(fileProperties.getFilePath());
        LOGGER.debug("List from file = {}", listOfModels);
        return listOfModels;
    }

    private void checkForExisting(ListOfModels listOfModels){

        for (Model model : listOfModels.getModelList()){
            if (getModel(model.getId())!=null){
                LOGGER.error("Model with such id already exists id={}", model.getId());
                throw new ConflictException("Model with such id already exists");
            }
        }
    }
}
