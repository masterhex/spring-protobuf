package com.mycompany.protobuf;

import com.googlecode.protobuf.format.JsonFormat;
import com.mycompany.protobuf.config.FileProperties;
import com.mycompany.protobuf.exception.ConflictException;
import com.mycompany.protobuf.mapper.ModelOuterClass.ListOfModels;
import com.mycompany.protobuf.mapper.ModelOuterClass.Model;
import com.mycompany.protobuf.util.FileUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ApplicationIntegrationTest {
    @LocalServerPort
    private int randomServerPort;
    private static final String tempFile = "list";
    private static String MODEL_URL;
    private static String tempPath = null;
    private JsonFormat jsonFormat;

    @MockBean
    private FileProperties fileProperties;

    @Before
    public void setUp() throws IOException {
        jsonFormat = new JsonFormat();
        MODEL_URL = "http://localhost:" + randomServerPort + "/protomodel";
        tempPath = System.getProperty("user.dir");
        clear();
        FileUtil.createFileIfNotExists(tempPath + File.separator + tempFile);
        doReturn(tempPath + File.separator + tempFile).when(fileProperties).getFilePath();
        Model model = Model.newBuilder().setId(1).setName("first").build();
        ListOfModels listOfModels = ListOfModels.newBuilder().addModel(model).build();
        String body = jsonFormat.printToString(listOfModels);
        executeHttpPostRequest(MODEL_URL, body);
    }

    @After
    public void clear() {
        try {
            FileUtil.deleteFileIfExists(tempPath + File.separator + tempFile);
        } catch (IOException ex){
            fail();
        }
    }

    @Test
    public void testWhenUsingPost_thenSucceed() throws IOException {

        Model model = Model.newBuilder().setId(2).setName("second").build();
        ListOfModels listOfModels = ListOfModels.newBuilder().addModel(model).build();
        String body = jsonFormat.printToString(listOfModels);
        executeHttpPostRequest(MODEL_URL, body).getEntity().getContent();

        InputStream responseStream = executeHttpGetRequest(MODEL_URL + "/2");
        String jsonOutput = IOUtils.toString(responseStream, StandardCharsets.UTF_8);
        assertResponseModel2(jsonOutput);
    }

    @Test
    public void testWhenUsingPostTheSameId_thenFailWithException409() throws IOException {
        // we have model with id 1 already in the file, see @Before
        Model model = Model.newBuilder().setId(1).setName("first").build();
        ListOfModels listOfModels = ListOfModels.newBuilder().addModel(model).build();
        String body = jsonFormat.printToString(listOfModels);
        HttpResponse response = executeHttpPostRequest(MODEL_URL, body);
        assertThat(response.getStatusLine().getStatusCode(), equalTo(409));
    }


    @Test
    public void testWhenUsingHttpClientGetModel_thenSucceed() throws IOException {
        InputStream responseStream = executeHttpGetRequest(MODEL_URL + "/1");
        String jsonOutput = IOUtils.toString(responseStream, StandardCharsets.UTF_8);
        assertResponseModel1(jsonOutput);
    }

    @Test
    public void testWhenUsingHttpClientGetList_thenSucceed() throws IOException {
        InputStream responseStream = executeHttpGetRequest(MODEL_URL);
        String jsonOutput = IOUtils.toString(responseStream, StandardCharsets.UTF_8);
        assertResponseModel1(jsonOutput);
    }

    private InputStream executeHttpGetRequest(String url) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(url);
        HttpResponse httpResponse = httpClient.execute(request);
        return httpResponse.getEntity().getContent();
    }

    private HttpResponse executeHttpPostRequest(String url, String body) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost request = new HttpPost(url);
        StringEntity entity = new StringEntity(body);
        request.setEntity(entity);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        HttpResponse httpResponse = httpClient.execute(request);
        return httpResponse;
    }


    private void assertResponseModel1(String response) {
        assertThat(response, containsString("id"));
        assertThat(response, containsString("name"));
        assertThat(response, containsString("1"));
        assertThat(response, containsString("first"));
    }

    private void assertResponseModel2(String response) {
        assertThat(response, containsString("id"));
        assertThat(response, containsString("name"));
        assertThat(response, containsString("2"));
        assertThat(response, containsString("second"));
    }
}